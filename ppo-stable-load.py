import gym
import os
import retro

from stable_baselines3 import PPO

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))

retro.data.Integrations.add_custom_path(os.path.join(SCRIPT_DIR, "custom"))

env = retro.make('megamanx-snes', use_restricted_actions=retro.Actions.DISCRETE, inttype=retro.data.Integrations.ALL, record='.')

def make_env():
    retro.data.Integrations.add_custom_path(os.path.join(SCRIPT_DIR, "custom"))
    env = make_retro(game="megamanx-snes", state=args.state, scenario=args.scenario, inttype=retro.data.Integrations.ALL)
    env = wrap_deepmind_retro(env)
    return env

#gym.make('CartPole-v1')

# Optional: PPO2 requires a vectorized environment to run

# the env is now wrapped automatically when passing it to the constructor
# env = DummyVecEnv([lambda: env])
#model = PPO('MlpPolicy', env, verbose=1)
#model.learn(total_timesteps=100000)
#model.save("mgmx-model-test")
model = PPO.load("mgmx-model-test")

obs = env.reset()
while True:
    action, _states = model.predict(obs)
    obs, rewards, done, info = env.step(action)
    if done:
        break
    #env.render()

#try:
#    env.unwrapped.record_movie(f"./ppo-bk2/ppo-test.bk2")
#    env.unwrapped.stop_record()

#except Exception as e:
#    print("unwrapped")
#    print(e)
#    try:
#        env.record_movie(f"./ppo-bk2/ppo-test.bk2")
#    except Exception as e:
#        print("not unwrapped")
#        print(e)

env.close()