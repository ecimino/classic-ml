FROM python:3.8

WORKDIR /
COPY requirements.txt .
RUN apt-get update -y && apt-get install -y libopenmpi-dev ffmpeg vim xvfb xserver-xephyr vnc4server cmake
RUN pip install --no-cache-dir -r requirements.txt
# git clone https://github.com/openai/retro.git && \
# git clone https://github.com/openai/baselines.git && \
# cd retro/ && pip install -e . && \
# cd baselines/ && pip install -e . && \
# pip install tensorflow && \
# pip install stable-baselines3

RUN git clone https://gitlab.com/ecimino/classic-ml.git