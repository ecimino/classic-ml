graph:
	docker cp b5f3255db3f0:/classic-ml/progress.txt ./perf/data/
	python3 perf/main.py

movie score:
	docker exec b5f3255db3f0 python /classic-ml/playback.py /classic-ml/bk2/{{ score }}.0.bk2
	docker cp b5f3255db3f0:/classic-ml/bk2/{{ score }}.0.mp4 ./perf/movies/
	open ./perf/movies/{{ score }}.0.mp4
