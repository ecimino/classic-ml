max_xpos = 128
prev_xpos = 128
function greater_xpos ()
  local max_delta = 0
  local delta = 0
  local penalty = 0

  if data.xpos > max_xpos then
    max_delta = data.xpos - max_xpos
    max_xpos = data.xpos
  end

  -- if data.xpos > prev_xpos then
  delta = data.xpos - prev_xpos
  -- else

  if data.xpos < 128 then
    if data.xpos < prev_xpos then
      penalty = -100
    end
  end

  if delta < 0 then
    delta = delta * 2
  end
    -- end
  prev_xpos = data.xpos
  return (max_delta * 100) + (delta * 10) + penalty 
end
