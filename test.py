#!/usr/local/bin/python3

import retro
import os

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))

def main():
        retro.data.Integrations.add_custom_path(
                os.path.join(SCRIPT_DIR, "custom")
        )
        print("megamanx-snes" in retro.data.list_games(inttype=retro.data.Integrations.ALL))
        env = retro.make("megamanx-snes", inttype=retro.data.Integrations.ALL)
        print(env)

if __name__ == "__main__":
        main()