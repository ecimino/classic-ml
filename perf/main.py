import matplotlib.pyplot as plt 
import numpy as np
import re

from numpy.core.numeric import roll 

content = []
with open('perf/data/progress.txt') as f:
    lines = f.readlines()
    try:
        content = [int(float(l.strip())) for l in lines]
    except Exception as e:
        for line in lines:
            line = line.strip()
            match = re.search("(.*=>.*?)?(-?[0-9]+)", line)
            if match:
                try:
                    content.append(int(match.group(2)))
                except:
                    ...

x = []
y = []

for idx, other in enumerate(content):
    x.append(idx)
    y.append(other)

x_np = np.array(x)
y_np = np.array(y)
m, b = np.polyfit(x_np, y_np, 1)
mean = np.mean(y_np)

divisor = int(len(x_np) / 50)
if divisor > 10:
    rolling = np.convolve(y_np, np.ones(divisor)/divisor, mode='valid')
    x_np_rolling_temp = [num for num in x_np if num % divisor == 0]
    if len(rolling) != len(x_np_rolling_temp):
        x_np_rolling_temp.pop(-1)
    x_np_rolling = np.array(x_np_rolling_temp)
    rolling = np.array([num for idx,num in enumerate(rolling) if idx % divisor == 0])
    plt.plot(x_np_rolling, rolling, 'g')


# plotting the points  
plt.plot(x_np, y_np, 'b,') 
plt.plot(x_np, m*x_np + b, 'r')



plt.xlabel('x - run number') 
plt.ylabel('y - reward score') 
# plt.text(0.02, 0.5, f"Mean: {mean}", fontsize=14, transform=plt.gcf().transFigure)

plt.title('Partial Performance') 
  
# plt.show() 
plt.savefig('perf/output/test.png')

print("Stats:")
print(f"\tNumber of Attempts: {len(content)}")
print(f"\tMin: {np.min(y_np)}")
print(f"\tMax: {np.max(y_np)}")
print(f"\tMean: {mean}")
print(f"\tMedian: {np.median(y_np)}")
print(f"\tStddev: {np.std(y_np)}")
